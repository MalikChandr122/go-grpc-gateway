## go-grpc-gateway-template

This is sample template using grpc-gateway concept,
which mean the communication inter services can use GRPC Protocol and with web client use REST API

In another references, web client also can communicate with GRPC Protocol directly, check this [link](https://jbrandhorst.com/post/grpc-in-the-browser/) for references

## Installation

#### Go Version 1.19

#### Install/download all dependencies on this project
```bash
$ go mod tidy
```

For simplicity, to build Protobuf ecosystem is auto-generate by tool called ``Buf``, check this [link](https://docs.buf.build/introduction) for installation
<p>

#### To test configuration of ``buf``

```bash
$ buf build
```

#### Build proto using buf

```bash
$ buf generate
```

## Running the app

```bash
$ make server
```

## API Documentation

[localhost:5000/swagger](http://localhost:5000/swagger)

