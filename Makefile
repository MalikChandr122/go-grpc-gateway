server:
	go run cmd/*.go
docker:
	docker build -t malikchandr/go-grpc-gateway-template . --build-arg PROJECT_NAME=go-grpc-gateway-template
run:
	docker run --rm --name go-grpc-gateway-template --network template-network -p 5000:5000 malikchandr/go-grpc-gateway-template
swagger-rest:
	swagger generate spec -m -o third_party/OpenAPI2/swagger/v1/swagger.yaml