package protocol

import (
	api "gitlab.com/MalikChandr122/go-grpc-gateway/proto/pb/v1"
	"gitlab.com/lib-go/gojunkyard/grpc/middleware"
	"gitlab.com/lib-go/gojunkyard/logger"
	"google.golang.org/grpc"
	"net"
)

// ServerGRPC
/**
*	This function for serve GRPC server
* 	Any service was registered in proto file must be register in here
*	Only register service that will be used on this server
**/
func ServerGRPC(dep *Dependencies, grpcL net.Listener) {
	opts := []grpc.ServerOption{}
	opts = middleware.AddLogger(logger.Log, opts)

	// create new grpc server
	server := grpc.NewServer(opts...)

	// register proto services
	api.RegisterFooServiceServer(server, dep.FooServerAPI)

	// start grpc server
	server.Serve(grpcL)
}
