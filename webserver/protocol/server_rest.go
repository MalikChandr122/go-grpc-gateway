package protocol

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	api "gitlab.com/MalikChandr122/go-grpc-gateway/proto/pb/v1"
	"gitlab.com/lib-go/gojunkyard/logger"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"net"
	"net/http"
)

func New(mux *http.ServeMux, grpcMux *runtime.ServeMux, httpMux http.Handler) *http.ServeMux {
	mux.HandleFunc("/ping", GetPing)

	mux.Handle("/api/rpc/", grpcMux)
	mux.Handle("/api/rest/", httpMux)
	return mux
}

// ServerHTTP ...
func ServerHTTP(handler http.Handler, httpL net.Listener) {
	server := http.Server{
		Handler: handler,
	}

	// start server
	server.Serve(httpL)
}

func RegisterEndpointForGRPC(port string) *runtime.ServeMux {
	// address of grpc server
	addr := "localhost" + port

	// create new server
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock()}

	// register proto services to provide REST API
	if err := api.RegisterFooServiceHandlerFromEndpoint(context.Background(), mux, addr, opts); err != nil {
		logger.Log.Fatal("Failed to start HTTP gateway", zap.String("reason", err.Error()))
	}
	return mux
}

func GetPing(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("pong"))
}
