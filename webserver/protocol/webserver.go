package protocol

import (
	"github.com/julienschmidt/httprouter"
	"github.com/soheilhy/cmux"
	api "gitlab.com/MalikChandr122/go-grpc-gateway/proto/pb/v1"
	"gitlab.com/MalikChandr122/go-grpc-gateway/service"
	"gitlab.com/MalikChandr122/go-grpc-gateway/webserver/protocol/rest"
	"gitlab.com/lib-go/gojunkyard/logger"
	"gitlab.com/lib-go/gojunkyard/rest/middleware"
	"gitlab.com/lib-go/gojunkyard/webserver"
	"go.uber.org/zap"
	"net"
	"net/http"
)

// Dependencies
/**
* 	Struct for collect API Services for GRPC Server
**/
type Dependencies struct {
	FooServerAPI api.FooServiceServer
}

// Serve
/**
*	This function to multiplex connections based on their payload
*	Serve grpc and http on the same TCP listener
**/
func RunServer(port string, restService service.Service, dep Dependencies) {
	// create listener
	l, err := net.Listen("tcp", port)
	if err != nil {
		logger.Log.Fatal("Failed to listener network", zap.String("reason", err.Error()))
	}
	// creat cmux
	m := cmux.New(l)
	httpL := m.Match(cmux.HTTP1Fast())
	grpcL := m.Match(cmux.HTTP2())

	go func() {
		// handler rest
		var httpHandler webserver.ServeMux
		httpHandler.ServeMux = New(
			http.NewServeMux(),
			RegisterEndpointForGRPC(port),
			rest.New(restService).Register(httprouter.New(), "/api/rest/v1"))
		httpHandler.Use(middleware.AddLogger)
		httpHandler.Use(middleware.AddRequestID)
		httpHandler.Use(middleware.CORSConfig)
		//httpHandler.Use(middleware.CSRF)
		//httpHandler.Use(middleware.Authorization)

		// deliver listener to server
		ServerHTTP(&httpHandler, httpL)
	}()
	go ServerGRPC(&dep, grpcL)

	// start server
	m.Serve()
}
