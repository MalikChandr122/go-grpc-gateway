package rest

import (
	"github.com/julienschmidt/httprouter"
	"gitlab.com/MalikChandr122/go-grpc-gateway/service"
	"net/http"
)

type API struct {
	Service service.Service
}

func New(service service.Service) *API {
	return &API{
		service,
	}
}

func (api *API) Register(route *httprouter.Router, prefix string) http.Handler {
	route.POST(prefix+"/authenticate", api.HandleLogin)

	route.ServeFiles(prefix+"/swagger/*filepath", api.getOpenAPIHandler())
	return route
}
