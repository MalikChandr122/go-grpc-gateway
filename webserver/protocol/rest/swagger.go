package rest

import (
	"gitlab.com/MalikChandr122/go-grpc-gateway/third_party"
	"gitlab.com/lib-go/gojunkyard/logger"
	"go.uber.org/zap"
	"io/fs"
	"mime"
	"net/http"
)

// getOpenAPIHandler serves an OpenAPI UI.
// Adapted from https://github.com/philips/grpc-gateway-example/blob/a269bcb5931ca92be0ceae6130ac27ae89582ecc/cmd/serve.go#L63
func (api *API) getOpenAPIHandler() http.FileSystem {
	mime.AddExtensionType(".svg", "image/svg+xml")
	// Use subdirectory in embedded files
	subFS, err := fs.Sub(third_party.OpenAPI2, "OpenAPI2")
	if err != nil {
		logger.Log.Error("couldn't create sub filesystem", zap.String("reason", err.Error()))
	}
	return http.FS(subFS)
}
