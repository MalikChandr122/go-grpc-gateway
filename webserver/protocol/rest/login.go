package rest

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/MalikChandr122/go-grpc-gateway/service"
	"net/http"
)

func (api *API) HandleLogin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var params service.LoginRequest

	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	t, err := api.Service.Login(&service.LoginRequest{
		Username: params.Username,
		Password: params.Password,
	})

	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	w.Write([]byte(t.Token))
}
