package client

import (
	api "gitlab.com/MalikChandr122/go-grpc-gateway/proto/pb/v1"
	"gitlab.com/lib-go/gojunkyard/logger"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// RegisterTestServiceClient
/**
*	Registering client service, for example the server service called Test Service
* 	Before registering service, make sure the proto file of server service already add to client service
* 	This function is used if client service need some data/process is only available in another service
* 	Communication between internal services, instead of use REST API
**/
func RegisterTestServiceClient(url string) api.TestServiceClient {
	logger.Log.Info("RegisterTestServiceClient listening to: " + url)
	cc, err := grpc.Dial(url, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		logger.Log.Fatal("RegisterTestServiceClient could not connect to: "+url, zap.String("reason", err.Error()))
	}
	return api.NewTestServiceClient(cc)
}
