package docs

import (
	"gitlab.com/MalikChandr122/go-grpc-gateway/service"
)

// swagger:route POST /authenticate login loginEndpoint
// Autentikasi.
// responses:
//   200: loginResponse

// Body.
// swagger:response loginResponse
type loginResponseWrapper struct {
	// in:body
	Body service.LoginResponse
}

// swagger:parameters loginEndpoint
type loginParamsWrapper struct {
	// request body.
	// in:body
	Body service.LoginRequest
}
