package service

import (
	"context"
	"fmt"
	"gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/entity"
	api "gitlab.com/MalikChandr122/go-grpc-gateway/proto/pb/v1"
	"gitlab.com/lib-go/gojunkyard/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s fooSvc) SelectAll(ctx context.Context, empty *emptypb.Empty) (*api.FooResponses, error) {
	/*msg, err := s.PrintMessageFromTestService()
	if err != nil {
		return nil, status.Errorf(codes.Unimplemented, err.Error())
	}
	logger.Log.Info(msg.Message)*/

	_foos, err := s.foo.SelectAll()

	if len(_foos) == 0 {
		return nil, status.Errorf(codes.NotFound, err.Error())
	}

	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &api.FooResponses{
		Code:    codes.OK.String(),
		Message: "Success",
		Data:    s.fooResponses(_foos),
	}, nil
}

func (s fooSvc) SelectByID(ctx context.Context, request *api.FooRequest) (*api.FooResponse, error) {
	_foo, err := s.foo.SelectByID(request.Id)

	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &api.FooResponse{
		Code:    codes.OK.String(),
		Message: "Success",
		Data:    s.fooResponse(_foo),
	}, nil
}

func (s fooSvc) CreateFoo(ctx context.Context, request *api.FooRequest) (*api.RequestResponse, error) {
	foo := entity.Foo{
		Title:       request.Title,
		Description: request.Description,
		CreatedBy:   request.CreatedBy,
	}

	err := s.foo.CreateFoo(&foo)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	logger.Log.Info(fmt.Sprintf("INSERT %v", &foo))

	return &api.RequestResponse{
		Id: foo.ID,
	}, nil
}

func (s fooSvc) UpdateFoo(ctx context.Context, request *api.FooRequest) (*api.UpdateResponse, error) {
	foo := entity.Foo{
		ID:          request.Id,
		Title:       request.Title,
		Description: request.Description,
		UpdatedBy:   request.UpdatedBy,
	}

	err := s.foo.UpdateFoo(&foo)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	logger.Log.Info(fmt.Sprintf("UPDATE %v", &foo))

	return &api.UpdateResponse{
		Id: foo.ID,
	}, nil
}

func (s fooSvc) DeleteFoo(ctx context.Context, request *api.FooRequest) (*api.DeleteResponse, error) {
	err := s.foo.DeleteFoo(request.Id)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &api.DeleteResponse{
		Id: request.Id,
	}, nil
}

func (s fooSvc) mustEmbedUnimplementedFooServiceServer() {}

func (s fooSvc) fooResponses(foos []*entity.Foo) (protoFoos []*api.Foo) {
	for _, foo := range foos {
		protoFoo := new(api.Foo)

		protoFoo.Id = foo.ID
		protoFoo.Title = foo.Title
		protoFoo.Description = foo.Description
		protoFoo.CreatedAt = timestamppb.New(foo.CreatedAt)
		protoFoo.CreatedBy = foo.CreatedBy
		protoFoo.UpdatedAt = timestamppb.New(foo.UpdatedAt)
		protoFoo.UpdatedBy = foo.UpdatedBy

		protoFoos = append(protoFoos, protoFoo)
	}
	return
}

func (s fooSvc) fooResponse(foo *entity.Foo) *api.Foo {
	protoFoo := new(api.Foo)
	protoFoo.Id = foo.ID
	protoFoo.Title = foo.Title
	protoFoo.Description = foo.Description
	protoFoo.CreatedAt = timestamppb.New(foo.CreatedAt)
	protoFoo.CreatedBy = foo.CreatedBy
	protoFoo.UpdatedAt = timestamppb.New(foo.UpdatedAt)
	protoFoo.UpdatedBy = foo.UpdatedBy
	return protoFoo
}

func (c fooSvc) PrintMessageFromTestService() (res *api.TestResponse, err error) {
	res, err = c.client.TestPrint(context.Background(), &emptypb.Empty{})
	return
}
