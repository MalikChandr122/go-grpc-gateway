package service

import "gitlab.com/lib-go/gojunkyard/rest/middleware"

type (
	LoginRequest struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	LoginResponse struct {
		Username string
		Token    string
	}
)

func (s svc) Login(request *LoginRequest) (*LoginResponse, error) {
	token, err := middleware.JWTAuthenticate(request.Username, "1")
	if err != nil {
		return nil, err
	}

	return &LoginResponse{
		Username: request.Username,
		Token:    token,
	}, nil
}
