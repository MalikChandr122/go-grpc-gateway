package service

import (
	foo "gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/repository"
	api "gitlab.com/MalikChandr122/go-grpc-gateway/proto/pb/v1"
	"gitlab.com/MalikChandr122/go-grpc-gateway/webserver/protocol/client"
)

// REST API
type svc struct {
	foo foo.Repository
}

type Service interface {
	Login(*LoginRequest) (*LoginResponse, error)
}

func NewRest(_foo foo.Repository) Service {
	return &svc{
		foo: _foo,
	}
}

// GRPC API
type fooSvc struct {
	foo    foo.Repository
	client api.TestServiceClient
	api.UnimplementedFooServiceServer
}

func NewFooServiceServer(_foo foo.Repository, clientSvcUrl ...string) api.FooServiceServer {
	return &fooSvc{
		foo:    _foo,
		client: client.RegisterTestServiceClient(clientSvcUrl[0]),
	}
}
