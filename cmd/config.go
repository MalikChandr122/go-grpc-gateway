package main

import (
	"gitlab.com/lib-go/gojunkyard/conn"
	"gitlab.com/lib-go/gojunkyard/env"
	"gitlab.com/lib-go/gojunkyard/logger"
	"time"
)

/**
* 	This struct used for collect value from .env
**/
type (
	config struct {
		App         appConfig        `envconfig:"APP"`
		Database    conn.DBConfig    `envconfig:"DATABASE"`
		Redis       conn.RedisConfig `envconfig:"REDIS"`
		RedisTTL    time.Duration    `envconfig:"REDIS_TTL"`
		RedisPrefix string           `envconfig:"REDIS_PREFIX"`
		Client      clientSvcConfig  `envconfig:"SVC_URL"`
	}

	appConfig struct {
		Env     string `envconfig:"ENV"`
		Version string `envconfig:"VERSION"`
		Port    string `envconfig:"PORT"`
	}

	clientSvcConfig struct {
		ClientSvc1 string `envconfig:"NAMESERVICES1"`
	}
)

const (
	appName    = "PROJECTNAME"
	appVersion = "1.0.0"
	logLevel   = 0
)

func loadConfig() *config {
	var cfg config

	/**
	*	Load .env file
	**/
	if err := env.LoadAndParse(appName, &cfg); err != nil {
		panic(err.Error())
	}
	return &cfg
}

func loadLogger() {
	/**
	* 	Initialize logger
	*	Log.Info	-> default logging
	*	Log.Error	-> logging for error level
	* 	Log.Fatal	-> logging for error level and stop the process
	* 	Log.Panic	-> logging for error level with print stack trace (like error position code) and stop the process
	**/
	if err := logger.Init(logLevel); err != nil {
		panic(err.Error())
	}
}
