package main

import (
	foo_mysql "gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/repository/mysql"
	foo_redis "gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/repository/redis"
	"gitlab.com/MalikChandr122/go-grpc-gateway/service"
	"strings"

	"gitlab.com/MalikChandr122/go-grpc-gateway/webserver/protocol"
	"gitlab.com/lib-go/gojunkyard/conn"
	"gitlab.com/lib-go/gojunkyard/logger"
)

func main() {
	var (
		cfg = loadConfig()
	)
	loadLogger()
	logger.Log.Info("Logger successfully initialize")
	logger.Log.Info("Env Successfully loaded: " + strings.ToUpper(cfg.App.Env) + " Mode")

	/**
	*	Initialize database SQL
	**/
	db, err := conn.InitDB(cfg.Database)
	if err != nil {
		panic(err.Error())
	}
	logger.Log.Info("Database Successfully initialize")

	/**
	* 	Initialize database NonSQL
	**/
	redis, err := conn.InitRedis(cfg.Redis)
	if err != nil {
		panic(err.Error())
	}
	logger.Log.Info("Redis Successfully initialize")

	/**
	*	Initialize API Services
	**/

	// Initialize Repository
	fooRepo := foo_mysql.New(db)
	logger.Log.Info("Foo Repository successfully initialized")
	fooRepoRedis := foo_redis.New(redis, fooRepo, cfg.RedisTTL, cfg.RedisPrefix)
	logger.Log.Info("Redis Repository successfully initialized")

	// Initialize GRPC Services
	fooService := service.NewFooServiceServer(fooRepoRedis, cfg.Client.ClientSvc1)

	/**
	* 	Initialize server and run it
	**/
	protocol.RunServer(
		cfg.App.Port,
		service.NewRest(
			fooRepoRedis,
		),
		protocol.Dependencies{
			FooServerAPI: fooService,
		},
	)
}
