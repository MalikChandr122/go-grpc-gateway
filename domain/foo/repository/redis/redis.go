package redis

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/entity"
	"gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/repository"
	"time"
)

// Redis ...
type Redis struct {
	redis       *redis.Pool
	repo        repository.Repository
	ttl         time.Duration
	redisPrefix string
}

// New ...
func New(redis *redis.Pool, repo repository.Repository, ttl time.Duration, redisPrefix string) *Redis {
	return &Redis{redis: redis, repo: repo, ttl: ttl, redisPrefix: redisPrefix}
}

func (r *Redis) fooIdKey(ID int64) string {
	return fmt.Sprintf("%s:foo:%d", r.redisPrefix, ID)
}

func (r *Redis) SelectAll() ([]*entity.Foo, error) {
	foo, err := r.repo.SelectAll()
	if err != nil {
		return nil, err
	}
	return foo, nil
}

func (r *Redis) SelectByID(ID int64) (*entity.Foo, error) {
	foo, err := r.getRedis(r.fooIdKey(ID))
	if err != nil {
		foo, err = r.repo.SelectByID(ID)
		if err != nil {
			return nil, err
		}

		byt, err := jsoniter.ConfigFastest.Marshal(foo)
		if err != nil {
			return nil, err
		}
		_ = r.setRedis(r.fooIdKey(ID), int(r.ttl.Seconds()), byt)
	}

	return foo, nil
}

func (r *Redis) CreateFoo(foo *entity.Foo) error {
	err := r.repo.CreateFoo(foo)
	if err != nil {
		return err
	}
	return nil
}

func (r *Redis) UpdateFoo(foo *entity.Foo) error {
	err := r.repo.UpdateFoo(foo)
	if err != nil {
		return err
	}
	return r.deleteRedis(r.fooIdKey(foo.ID))
}

func (r *Redis) DeleteFoo(ID int64) error {
	err := r.repo.DeleteFoo(ID)
	if err != nil {
		return err
	}
	return r.deleteRedis(r.fooIdKey(ID))
}
