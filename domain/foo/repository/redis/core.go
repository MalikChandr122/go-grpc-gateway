package redis

import (
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/entity"
)

func (r *Redis) selectRedis(key string) (foos []*entity.Foo, err error) {
	conn := r.redis.Get()
	defer conn.Close()

	b, err := redis.Bytes(conn.Do("GET", key))
	err = json.Unmarshal(b, &foos)
	return
}

func (r *Redis) getRedis(key string) (foo *entity.Foo, err error) {
	conn := r.redis.Get()
	defer conn.Close()

	b, err := redis.Bytes(conn.Do("GET", key))
	err = json.Unmarshal(b, &foo)
	return
}

func (r *Redis) setRedis(key string, expired int, data []byte) (err error) {
	conn := r.redis.Get()
	defer conn.Close()

	_, err = conn.Do("SET", key, data)
	_, err = conn.Do("EXPIRE", key, expired)
	return
}

func (r *Redis) deleteRedis(key string) error {
	conn := r.redis.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", key)
	return err
}
