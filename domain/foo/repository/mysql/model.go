package mysql

import (
	"gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/entity"
	"time"
)

// Foo ...
type Foo struct {
	ID          int64     `db:"id"`
	Title       string    `db:"title"`
	Description string    `db:"description"`
	CreatedAt   time.Time `db:"created_at"`
	CreatedBy   string    `db:"created_by"`
	UpdatedAt   time.Time `db:"updated_at"`
	UpdatedBy   string    `db:"updated_by"`
}

func (m *MySQL) entitiesFoo(foos []Foo) []*entity.Foo {
	entities := make([]*entity.Foo, 0, len(foos))
	for _, f := range foos {
		entities = append(entities, &entity.Foo{
			ID:          f.ID,
			Title:       f.Title,
			Description: f.Description,
			CreatedAt:   f.CreatedAt,
			CreatedBy:   f.CreatedBy,
			UpdatedAt:   f.UpdatedAt,
			UpdatedBy:   f.UpdatedBy,
		})
	}
	return entities
}

func (m *MySQL) entityFoo(f Foo) *entity.Foo {
	entities := &entity.Foo{
		ID:          f.ID,
		Title:       f.Title,
		Description: f.Description,
		CreatedAt:   f.CreatedAt,
		CreatedBy:   f.CreatedBy,
		UpdatedAt:   f.UpdatedAt,
		UpdatedBy:   f.UpdatedBy,
	}
	return entities
}
