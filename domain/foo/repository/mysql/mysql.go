package mysql

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/entity"
	"time"
)

// MySQL ...
type MySQL struct {
	db *sqlx.DB
}

// New ...
func New(db *sqlx.DB) *MySQL {
	return &MySQL{db: db}
}

func (m *MySQL) SelectAll() ([]*entity.Foo, error) {
	var foos []Foo
	err := m.db.Select(&foos, `
		SELECT
			id,
			title,
			description,
			created_at,
			created_by,
		    updated_at,
		    updated_by
		FROM
			tb_foo
		where 
		    deleted_at is null
	`)

	if err != nil {
		return nil, err
	}

	return m.entitiesFoo(foos), nil
}

func (m *MySQL) SelectByID(ID int64) (*entity.Foo, error) {
	var foo Foo
	err := m.db.Get(&foo, `
		SELECT
			id,
			title,
			description,
			created_at,
			created_by,
		    updated_at,
		    updated_by
		FROM 
			tb_foo
		WHERE
		    id = ? and
		    deleted_at is null
	`, ID)

	if err != nil {
		return nil, err
	}

	return m.entityFoo(foo), nil
}

func (m *MySQL) CreateFoo(foo *entity.Foo) error {
	timeNow := time.Now()
	foo.CreatedAt = timeNow
	foo.UpdatedAt = timeNow

	query := `
		INSERT INTO tb_foo
			(title, description, created_at, created_by, updated_at, updated_by)
		VALUES 
		    (
		     :title,
		     :description,
		     :created_at,
		     :created_by,
		     :updated_at,
		     :updated_by
		    )
	`

	r, err := m.db.NamedExec(query, &Foo{
		ID:          foo.ID,
		Title:       foo.Title,
		Description: foo.Description,
		CreatedAt:   foo.CreatedAt,
		CreatedBy:   foo.CreatedBy,
		UpdatedAt:   foo.UpdatedAt,
		UpdatedBy:   foo.CreatedBy,
	})

	if err != nil {
		return err
	}

	lastID, err := r.LastInsertId()
	foo.ID = lastID

	return err
}

func (m *MySQL) UpdateFoo(foo *entity.Foo) error {
	foo.UpdatedAt = time.Now()

	query := `
		UPDATE tb_foo SET
			title = :title,
			description = :description,
			updated_at = :updated_at,
			updated_by = :updated_by
		WHERE
		    id = :id
	`

	r, err := m.db.NamedExec(query, &Foo{
		ID:          foo.ID,
		Title:       foo.Title,
		Description: foo.Description,
		UpdatedAt:   foo.UpdatedAt,
		UpdatedBy:   foo.UpdatedBy,
	})

	if err != nil {
		return err
	}

	num, err := r.RowsAffected()
	if err != nil {
		return err
	}

	if num == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (m *MySQL) DeleteFoo(ID int64) error {
	r, err := m.db.Exec(`
		DELETE FROM tb_foo 
		WHERE id = ?
	`, ID)

	if err != nil {
		return err
	}

	num, err := r.RowsAffected()
	if err != nil {
		return err
	}

	if num == 0 {
		return sql.ErrNoRows
	}

	return err
}
