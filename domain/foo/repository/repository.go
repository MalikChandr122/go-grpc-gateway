package repository

import (
	"gitlab.com/MalikChandr122/go-grpc-gateway/domain/foo/entity"
)

// Repository ...
type Repository interface {
	SelectAll() ([]*entity.Foo, error)
	SelectByID(ID int64) (*entity.Foo, error)
	CreateFoo(*entity.Foo) error
	UpdateFoo(*entity.Foo) error
	DeleteFoo(ID int64) error
}
