package entity

import (
	"time"
)

// Foo ...
type Foo struct {
	ID          int64
	Title       string
	Description string
	CreatedAt   time.Time
	CreatedBy   string
	UpdatedAt   time.Time
	UpdatedBy   string
}
