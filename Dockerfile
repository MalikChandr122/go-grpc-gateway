## Build image
FROM golang:1.19-alpine AS build

ARG PROJECT_NAME

WORKDIR /app/$PROJECT_NAME
COPY . .

RUN go mod vendor

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o $PROJECT_NAME ./cmd/

## Base image
FROM alpine:latest

ARG PROJECT_NAME

WORKDIR /app/$PROJECT_NAME
COPY --from=build /app/$PROJECT_NAME/$PROJECT_NAME /go-grpc-gateway-template
COPY .env.production .env

CMD /go-grpc-gateway-template